// app/models/todo.js
// load mongoose since we need it to define a model
var mongoose = require('mongoose');

module.exports = mongoose.model('Todo', {
    title: String,
    price: Number,
    email: String,
    details: String,
    imgUrl: String

});