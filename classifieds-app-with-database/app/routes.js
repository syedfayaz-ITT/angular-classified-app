// app/routes.js
// load the todo model
var Todo = require('./models/todo');

// expose the routes to our app with module.exports
module.exports = function(app) {

    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/todos', function(req, res) {

        // use mongoose to get all todos in the database
        Todo.find(function(err, todos) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(todos); // return all todos in JSON format
        });
    });

    // create todo and send back all todos after creation
    app.post('/api/todos', function(req, res) {

        // create a todo, information comes from AJAX request from Angular
        Todo.create({
            title: req.body.title,
            price: req.body.price,
            email: req.body.email,
            details: req.body.details,
            imgUrl: req.body.imgUrl

        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });

    });




    app.post('/api/sendmail', function(req, res) {

        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'neutronmane@gmail.com',
                pass: '8147496987'
            }
        });

        console.log("FROM::" + req.body.email);
        let mailOptions = {
            from: 'neutronmane@gmail.com',
            to: req.body.email,
            subject: ('Your item %s is requested to buy', req.body.title),
            text: 'I am testing Nodemailer to send email.',
        };

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888'); // Change this to your Angular 2 port number
        res.setHeader('Access-Control-Request-Method', '*');
        res.setHeader('Access-Control-Allow-Methods', 'POST');
        res.setHeader('Access-Control-Allow-Headers', '*');

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });

    });

};