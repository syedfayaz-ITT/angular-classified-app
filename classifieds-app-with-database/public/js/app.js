var myApp = angular.module("classifiedApp", ["ngRoute",'todoController', 'todoService']);

myApp.config(function($routeProvider) {
  $routeProvider
    .when("/shop", {
      templateUrl: "views/shop.html",
      controller: "ShopController"
    })
    .when("/sell", {
      templateUrl: "views/sell.html",
      controller: "SellController"
    })
      .when("/cart", {
      templateUrl: "views/cart.html",
      controller: "CartController"
    })
  .otherwise({
    redirectTo: "/shop"
  });
});






