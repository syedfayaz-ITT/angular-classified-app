// js/controllers/main.js
angular.module('todoController', [])

    .controller('mainController', function($scope, $http) {
        $scope.newItem = {};
        $scope.names = [{
            name: "Select Category",
            value: ""
        }, {
            name: "Books",
            value: "Books"
        }, {
            name: "Electronics",
            value: "Electronics"
        }, {
            name: "Others",
            value: "Others"
        }];
        $scope.newItem.category = $scope.names[0];

        // when landing on the page, get all todos and show them
        $http.get('/api/todos')
            .success(function(data) {
                $scope.todos = data;
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });

        // when submitting the add form, send the text to the node API
        $scope.createTodo = function() {
            $http.post('/api/todos', $scope.newItem)
                .success(function(data) {
                    $scope.newItem = {}; // clear the form so our user is ready to enter another
                    $scope.todos = data;
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });

        };

        $scope.sendEmail = function(item) {
            $http.post('/api/sendmail', item)
                .success(function(data) {
                    console.log("Success Email" + data);
                    alert("Email Sent Successfully" + data);
                })
                .error(function(data) {
                    console.log("Error in sending mail");
                });
            alert("Order sent to : " + item.email);
        };

        $scope.resetForm = function() {
            $scope.newItem = {
                "title": "",
                "price": "",
                "email": "",
                "details": "",
                "imgUrl": ""
            };
            // 'myForm' is the name of the <form> tag.
            $scope.form.myForm.$setPristine();
        };

        // delete a todo after checking it
        $scope.deleteTodo = function(id) {
            $http.delete('/api/todos/' + id)
                .success(function(data) {
                    $scope.todos = data;
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        };

    });

myApp.controller("HeaderController", function($scope, $location) {
    $scope.appDetails = {};
    $scope.appDetails.title = "Buy and Sell";
    $scope.appDetails.tagline = "Sell used products";

    $scope.nav = {};
    $scope.nav.isActive = function(path) {
        if (path === $location.path()) {
            return true;
        }

        return false;
    }
});

myApp.controller("ShopController", function($scope, shopService, kartService) {
    $scope.items = shopService.getItems();

    $scope.addToKart = function(item) {
        kartService.addToKart(item);
    }
});

myApp.controller("CartController", function($scope, kartService) {
    $scope.kart = kartService.getKart();

    $scope.buy = function(item) {
        //console.log("item: ", item);
        kartService.buy(item);
    }
});