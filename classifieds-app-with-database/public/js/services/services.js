// js/services/todos.js
angular.module('todoService', [])

    .factory('Todos', function($http) {
        return {
            get: function() {
                return $http.get('/api/todos');
            },
            create: function(todoData) {
                return $http.post('/api/todos', todoData);
            },
            delete: function(id) {
                return $http.delete('/api/todos/' + id);
            },
            mail: function(item) {
                return $http.post('/api/sendmail/', item);
            }

        }
    });

myApp.factory("kartService", function() {
    var kart = [];

    return {
        getKart: function() {
            return kart;
        },
        addToKart: function(item) {
            kart.push(item);
            alert("Order sent to : " + item.email);

        },
        addNewItem: function(item) {

        },
        buy: function(item) {
            alert("Order sent to : " + item.email);
        }
    }
});